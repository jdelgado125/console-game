using System;

namespace ConsoleGame
{
  class Game : SuperGame
  {
    /*Upates the players position*/
    public new static void UpdatePosition(string keyPressed, out int xPos, out int yPos)
    {
        xPos = 0;
        yPos = 0;

        switch (keyPressed)
        {
            case "UpArrow":
                yPos--;
                break;
            case "DownArrow":
                yPos++;
                break;
            case "LeftArrow":
                xPos--;
                break;
            case "RightArrow":
                xPos++;
                break;
            default:
                xPos = 0;
                yPos = 0;
                break;
        }

    }

    public new static char UpdateCursor(string keyPressed)
    {
        switch (keyPressed)
        {
            case "UpArrow": return '^';
            case "DownArrow": return 'v';
            case "LeftArrow": return '<';
            case "RightArrow": return '>';
            default: return '>';
        }
    }

    public new static int KeepInBounds(int coord, int maxVal)
    {
        if(coord >= maxVal)
        {
            return maxVal - 1;
        }
        else if(coord < 0)
        {
            return 0;
        }
        else
        {
            return coord;
        }
    }

    public new static bool DidScore(int playerXPos, int playerYPos, int fruitXPos, int fruitYPos)
    {
        if(playerXPos == fruitXPos && playerYPos == fruitYPos)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
  }
}